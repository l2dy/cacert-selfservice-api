/*
Copyright 2019-2021 Jan Dittberner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this program except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gopkg.in/gorp.v2"

	"git.cacert.org/cacert-selfservice-api/selfservice-api/models"
	"git.cacert.org/cacert-selfservice-api/selfservice-api/services"
)

type ResetPasswordPayload struct {
	EmailAddress          string `json:"email"`
	RequesterEmailAddress string `json:"requesterEmailAddress"`
	IsAdmin               bool   `json:"isAdmin"`
	NewPassword           string `json:"newPassword"`
	Remarks               string `json:"remarks"`
}

type ResetPasswordResponse struct {
	Success  bool   `json:"success"`
	Message  string `json:"message"`
	Username string `json:"username"`
}

type EmailPasswordHandler struct {
	dovecotHashAlgorithm string
	db                   *models.DbHandler
}

func NewEmailPasswordHandler(dovecotHashAlgorithm string, db *models.DbHandler) *EmailPasswordHandler {
	return &EmailPasswordHandler{dovecotHashAlgorithm: dovecotHashAlgorithm, db: db}
}

func (h *EmailPasswordHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPut {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)

		return
	}

	payloadBytes, clientID, err := authenticatePayload(r)
	if err != nil {
		handleAuthenticationError(w, err)

		return
	}

	request := &ResetPasswordPayload{}
	if err = json.Unmarshal(payloadBytes, request); err != nil {
		log.Warnf("could not unmarshal request from client %s: %v", *clientID, err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return
	}

	user, err := verifyUser(request, h.db.DbMap)
	if err != nil {
		log.Error(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	if user == nil {
		respondWithPayload(w, http.StatusOK, &ResetPasswordResponse{
			Success:  false,
			Message:  "User not found",
			Username: "",
		})

		return
	}

	passwordHash, err := services.DovecotHash(request.NewPassword, h.dovecotHashAlgorithm)
	if err != nil {
		log.Error(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	user.Password = passwordHash

	_, err = h.db.DbMap.Update(user)
	if err != nil {
		log.Error(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	if request.IsAdmin {
		if err := models.WriteAdminLog(
			h.db.DbMap,
			models.LogTypePasswordReset,
			request.RequesterEmailAddress,
			user.Username,
			request.Remarks); err != nil {
			log.Errorf("error writing admin log entry: %v", err)
		}
	} else {
		services.NotifyMailChannel <- services.NewPasswordResetNotification(
			request.EmailAddress,
			user.Username,
			r.Header.Get("Original-Addr"),
		)
	}

	respondWithPayload(w, http.StatusOK, &ResetPasswordResponse{
		Success:  true,
		Message:  fmt.Sprintf("New password for %s has been set", user.Username),
		Username: user.Username,
	})
}

func verifyUser(payload *ResetPasswordPayload, dbMap *gorp.DbMap) (*models.User, error) {
	var (
		user *models.User
		err  error
	)

	if user, err = models.FindUserByEmailAddress(dbMap, payload.EmailAddress); err != nil {
		return nil, fmt.Errorf("could not find users by email address: %w", err)
	}

	return user, nil
}
