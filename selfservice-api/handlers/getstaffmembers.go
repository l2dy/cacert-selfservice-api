/*
Copyright 2019-2021 Jan Dittberner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this program except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package handlers

import (
	"net/http"

	log "github.com/sirupsen/logrus"

	"git.cacert.org/cacert-selfservice-api/selfservice-api/models"
)

type StaffMember struct {
	Username string `json:"username"`
	RealName string `json:"realName"`
	Role     string `json:"role"`
}

type GetStaffMemberResponse struct {
	Members []StaffMember `json:"members"`
}

type GetStaffMembersHandler struct {
	db *models.DbHandler
}

func NewGetStaffMembersHandler(db *models.DbHandler) *GetStaffMembersHandler {
	return &GetStaffMembersHandler{db: db}
}

func (h *GetStaffMembersHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)

		return
	}

	_, _, err := authenticatePayload(r)
	if err != nil {
		handleAuthenticationError(w, err)

		return
	}

	staff := make([]StaffMember, 0)

	members, err := models.GetAllStaffMembers(h.db.DbMap)
	if err != nil {
		log.Errorf("could not query database for staff members: %v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	for _, member := range members {
		staff = append(staff, StaffMember{
			Username: member.Username,
			RealName: member.RealName,
			Role:     member.Role,
		})
	}

	respondWithPayload(w, http.StatusOK, &GetStaffMemberResponse{Members: staff})
}
