/*
Copyright 2019-2021 Jan Dittberner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this program except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package handlers

import (
	"net/http"
	"sort"

	log "github.com/sirupsen/logrus"

	"git.cacert.org/cacert-selfservice-api/selfservice-api/models"
)

type Alias struct {
	Alias   string   `json:"alias"`
	Targets []string `json:"targets"`
}

type GetAliasesResponse struct {
	Aliases []Alias `json:"aliases"`
}

type GetAliasesHandler struct {
	db *models.DbHandler
}

func NewGetAliasesHandler(db *models.DbHandler) *GetAliasesHandler {
	return &GetAliasesHandler{db: db}
}

func (h *GetAliasesHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)

		return
	}

	_, _, err := authenticatePayload(r)
	if err != nil {
		handleAuthenticationError(w, err)

		return
	}

	aliasMap := make(map[string][]string)

	allAliases, err := models.GetAllAliases(h.db.DbMap)
	if err != nil {
		log.Errorf("could not query database for aliases: %v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	for _, alias := range allAliases {
		if _, ok := aliasMap[alias.Alias]; !ok {
			aliasMap[alias.Alias] = make([]string, 0)
		}

		aliasMap[alias.Alias] = append(aliasMap[alias.Alias], alias.User)
	}

	aliases := make([]Alias, 0)
	mapKeys := make([]string, 0)

	for k := range aliasMap {
		mapKeys = append(mapKeys, k)
	}

	sort.Strings(mapKeys)

	for _, k := range mapKeys {
		alias := Alias{Alias: k}

		sort.Strings(aliasMap[k])

		alias.Targets = aliasMap[k]

		aliases = append(aliases, alias)
	}

	respondWithPayload(w, http.StatusOK, &GetAliasesResponse{Aliases: aliases})
}
