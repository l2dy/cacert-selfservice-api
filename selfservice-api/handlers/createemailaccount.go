/*
Copyright 2020-2021 Jan Dittberner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this program except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package handlers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"

	"git.cacert.org/cacert-selfservice-api/selfservice-api/models"
	"git.cacert.org/cacert-selfservice-api/selfservice-api/services"
)

const InitialPassword = "*initial*"

type CreateEmailAccountPayload struct {
	UserName              string `json:"username"`
	ContactEmail          string `json:"contactEmail"`
	RequesterEmailAddress string `json:"requestEmailAddress"`
	RealName              string `json:"realName"`
	FullNameAlias         string `json:"fullNameAlias"`
	FullNameAlias2        string `json:"fullNameAlias2"`
	Role                  string `json:"role"`
	Remarks               string `json:"remarks"`
	InitialPasswordURL    string `json:"initial_password_url"`
	ValidHours            int    `json:"url_valid_hours"`
}

type CreateEmailAccountResponse struct {
	Success        bool     `json:"success"`
	Message        string   `json:"message"`
	Username       string   `json:"username"`
	EmailAddresses []string `json:"emailAddresses"`
}

type CreateEmailAccountHandler struct {
	db *models.DbHandler
}

func NewCreateEmailAccountHandler(db *models.DbHandler) *CreateEmailAccountHandler {
	return &CreateEmailAccountHandler{db: db}
}

func (c *CreateEmailAccountHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)

		return
	}

	payloadBytes, clientID, err := authenticatePayload(r)
	if err != nil {
		handleAuthenticationError(w, err)

		return
	}

	request := &CreateEmailAccountPayload{}
	if err = json.Unmarshal(payloadBytes, request); err != nil {
		log.Warnf("could not unmarshal request from client %s: %v", *clientID, err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return
	}

	// check for existing user
	addresses := make([]string, 0)
	addresses = append(addresses, request.UserName)
	addresses = append(addresses, request.FullNameAlias)

	if request.FullNameAlias2 != "" {
		addresses = append(addresses, request.FullNameAlias2)
	}

	if exists, err := models.CheckUserExistsByUserNames(c.db.DbMap, addresses); err != nil {
		log.Error(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	} else if exists {
		log.Warnf("tried to create duplicate user with one of %s for %s", strings.Join(addresses, ", "), request.RealName)
		respondWithPayload(w, http.StatusConflict, &CreateEmailAccountResponse{
			Success:        false,
			Message:        "The user name or full name alias is already in use",
			Username:       "",
			EmailAddresses: addresses,
		})

		return
	}

	// add database entry
	user, err := c.writeUser(request)
	if err != nil {
		log.Error(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	log.Infof("created user %s", user.Username)

	if err = services.DovecotCreateUserMailbox(user.Username); err != nil {
		log.Errorf("could not create mailbox for user %s: %v", user.Username, err)
	}

	if err = models.WriteAdminLog(
		c.db.DbMap, models.LogTypeCreateAccount, request.RequesterEmailAddress, user.Username, request.Remarks,
	); err != nil {
		log.Errorf("error writing admin log entry: %v", err)
	}

	// TODO: implement one time password self service

	// send mail to contact address
	services.NotifyMailChannel <- services.NewEmailAccountCreateNotification(
		request.RealName,
		request.ContactEmail,
		request.UserName,
		addresses,
		user.Domain,
		request.InitialPasswordURL,
		request.ValidHours,
	)
	// send mail to email admins
	services.NotifyMailChannel <- services.NewEmailAccountCreateAdminNotification(
		request.RealName,
		request.UserName,
		request.ContactEmail,
		addresses,
		user.Domain,
		request.RequesterEmailAddress,
		request.Remarks,
	)

	respondWithPayload(w, http.StatusOK, &CreateEmailAccountResponse{
		Success:        true,
		Message:        fmt.Sprintf("New email account for %s has been created", request.RealName),
		Username:       request.UserName,
		EmailAddresses: addresses,
	})
}

func (c *CreateEmailAccountHandler) writeUser(request *CreateEmailAccountPayload) (*models.User, error) {
	user := &models.User{
		Username: request.UserName,
		FullNameAlias: sql.NullString{
			String: request.FullNameAlias,
			Valid:  true,
		},
		FullNameAlias2: sql.NullString{
			String: request.FullNameAlias2,
			Valid:  request.FullNameAlias2 != "",
		},
		RealName: request.RealName,
		Password: InitialPassword,
		Role:     request.Role,
		Domain:   "cacert.org",
	}

	if err := models.CreateUser(c.db.DbMap, user); err != nil {
		return nil, fmt.Errorf("could not create user: %w", err)
	}

	return user, nil
}
