/*
Copyright 2020-2021 Jan Dittberner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this program except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gopkg.in/gorp.v2"

	"git.cacert.org/cacert-selfservice-api/selfservice-api/models"
	"git.cacert.org/cacert-selfservice-api/selfservice-api/services"
)

type InitialPasswordPayload struct {
	Username     string `json:"username"`
	NewPassword  string `json:"newPassword"`
	ContactEmail string `json:"email"`
}

type InitialPasswordResponse struct {
	Success        bool     `json:"success"`
	Message        string   `json:"message"`
	EmailAddresses []string `json:"email_addresses"`
}

type InitialPasswordHandler struct {
	dovecotHashAlgorithm string
	db                   *models.DbHandler
}

func NewInitialPasswordHandler(dovecotHashAlgorithm string, db *models.DbHandler) *InitialPasswordHandler {
	return &InitialPasswordHandler{dovecotHashAlgorithm: dovecotHashAlgorithm, db: db}
}

func (h *InitialPasswordHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)

		return
	}

	payloadBytes, clientID, err := authenticatePayload(r)
	if err != nil {
		handleAuthenticationError(w, err)

		return
	}

	request := &InitialPasswordPayload{}
	if err = json.Unmarshal(payloadBytes, request); err != nil {
		log.Warnf("could not unmarshal request from client %s: %v", *clientID, err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return
	}

	user, initial, err := verifyInitialUser(request, h.db.DbMap)
	if err != nil {
		log.Error(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}

	if user == nil {
		respondWithPayload(w, http.StatusNotFound, &InitialPasswordResponse{
			Success: false,
			Message: "User not found.",
		})

		return
	}

	if !initial {
		respondWithPayload(w, http.StatusConflict, &InitialPasswordResponse{
			Success: false,
			Message: "Password has already been set. Use the regular password reset process.",
		})

		return
	}

	passwordHash, err := services.DovecotHash(request.NewPassword, h.dovecotHashAlgorithm)
	if err != nil {
		log.Error(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	user.Password = passwordHash

	_, err = h.db.DbMap.Update(user)
	if err != nil {
		log.Error(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	services.NotifyMailChannel <- services.NewPasswordResetNotification(
		request.ContactEmail,
		user.Username,
		r.Header.Get("Original-Addr"),
	)

	respondWithPayload(w, http.StatusOK, &InitialPasswordResponse{
		Success:        true,
		Message:        fmt.Sprintf("Initial password for %s has been set", user.Username),
		EmailAddresses: user.GetEmailAddresses(),
	})
}

func verifyInitialUser(request *InitialPasswordPayload, dbMap *gorp.DbMap) (*models.User, bool, error) {
	var (
		user *models.User
		err  error
	)

	if user, err = models.FindUserByUsername(dbMap, request.Username); err != nil {
		return nil, false, fmt.Errorf("could not find user by username: %w", err)
	}

	return user, user.Password == InitialPassword, nil
}
