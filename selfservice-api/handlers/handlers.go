/*
Copyright 2019-2021 Jan Dittberner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this program except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// The handlers package contains the HTTP handlers for the API endpoints.
package handlers

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/asn1"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"math/big"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"

	"git.cacert.org/cacert-selfservice-api/selfservice-api/models"
)

const (
	apiAliases         = "/aliases"
	apiEmailAccount    = "/email-account"
	apiEmailAddresses  = "/email-addresses"
	apiEmailPassword   = "/email-password"
	apiHealth          = "/health"
	apiInitialPassword = "/initial-password"
	apiStaffMembers    = "/staff-members"
)

var clientKeys map[string]*ecdsa.PublicKey

func RegisterClient(id string, key string) {
	if clientKeys == nil {
		clientKeys = make(map[string]*ecdsa.PublicKey)
	}

	decodedBytes, _ := pem.Decode([]byte(key))

	clientPublicKey, err := x509.ParsePKIXPublicKey(decodedBytes.Bytes)
	if err != nil {
		log.Errorf("could not parse public key for client %s: %v", id, err)
	}

	clientKeys[id] = clientPublicKey.(*ecdsa.PublicKey)
}

func RegisterHandlers(db *models.DbHandler, dovecotHashAlgorithm string) {
	http.Handle(apiHealth, NewHealthHandler(db))
	http.Handle(apiAliases, NewGetAliasesHandler(db))
	http.Handle(apiEmailAddresses, NewGetEmailAddressesHandler(db))
	http.Handle(apiEmailPassword, NewEmailPasswordHandler(dovecotHashAlgorithm, db))
	http.Handle(apiStaffMembers, NewGetStaffMembersHandler(db))
	http.Handle(apiEmailAccount, NewCreateEmailAccountHandler(db))
	http.Handle(apiInitialPassword, NewInitialPasswordHandler(dovecotHashAlgorithm, db))
}

type Unauthenticated struct {
	Message string
}

func (e Unauthenticated) Error() string {
	if e.Message != "" {
		return fmt.Sprintf("client could not be authenticated: %s", e.Message)
	}

	return "client could not be authenticated"
}

func authenticatePayload(r *http.Request) ([]byte, *string, error) {
	clientID := r.Header.Get("Client-ID")
	if clientID == "" {
		return nil, nil, Unauthenticated{Message: "no Client-ID header in request"}
	}

	clientKey, found := clientKeys[clientID]
	if !found {
		return nil, nil, Unauthenticated{
			Message: fmt.Sprintf("no client key found for client with ID %s", clientID),
		}
	}

	var err error

	buf := new(bytes.Buffer)
	if _, err = io.Copy(buf, r.Body); err != nil {
		return nil, nil, fmt.Errorf("could not copy request body: %w", err)
	}

	payload := buf.Bytes()
	sha256Hash := sha256.New()

	_, err = sha256Hash.Write([]byte(r.URL.Path))
	if err != nil {
		return nil, nil, fmt.Errorf("could not write to sha256Hash: %w", err)
	}

	if r.URL.RawQuery != "" {
		_, err = sha256Hash.Write([]byte("?"))
		if err != nil {
			return nil, nil, fmt.Errorf("could not write to sha256Hash: %w", err)
		}

		_, err = sha256Hash.Write([]byte(r.URL.RawQuery))
		if err != nil {
			return nil, nil, fmt.Errorf("could not write to sha256Hash: %w", err)
		}
	}

	_, err = sha256Hash.Write(payload)
	if err != nil {
		return nil, nil, fmt.Errorf("could not write to sha256Hash: %w", err)
	}

	checkSum := sha256Hash.Sum(make([]byte, 0))

	signature := r.Header.Get("Hash-Signature")
	if strings.TrimSpace(signature) == "" {
		return nil, nil, Unauthenticated{Message: "no signature header found"}
	}

	signatureBytes, err := base64.RawStdEncoding.DecodeString(signature)
	if err != nil {
		return nil, nil, Unauthenticated{
			Message: fmt.Sprintf("could not decode signature '%s': %v", signature, err),
		}
	}

	type ecdsaSignature struct {
		R, S *big.Int
	}

	sig := &ecdsaSignature{}

	_, err = asn1.Unmarshal(signatureBytes, sig)
	if err != nil {
		return nil, nil, Unauthenticated{
			Message: fmt.Sprintf("could not unmarshal ECDSA signature: %v", err),
		}
	}

	if !ecdsa.Verify(clientKey, checkSum, sig.R, sig.S) {
		return nil, nil, Unauthenticated{
			Message: fmt.Sprintf("signature from client %s is not valid, rejecting request", clientID),
		}
	}

	return payload, &clientID, nil
}

func handleAuthenticationError(w http.ResponseWriter, err error) {
	var target *Unauthenticated
	if errors.As(err, target) {
		log.Warn(err)
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)

		return
	}

	log.Error(err)
	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

func respondWithPayload(w http.ResponseWriter, statusCode int, payload interface{}) {
	data, err := json.Marshal(payload)
	if err != nil {
		log.Errorf("marshaling of response JSON failed: %v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	_, err = w.Write(data)
	if err != nil {
		log.Errorf("could not write response data: %v", err)
	}
}

type healthResponse struct {
	Healthy bool   `json:"healthy"`
	Reason  string `json:"reason"`
}

type healthHandler struct {
	db *models.DbHandler
}

func NewHealthHandler(db *models.DbHandler) http.Handler {
	return &healthHandler{db: db}
}

func (h *healthHandler) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	if err := h.db.DbMap.Db.Ping(); err != nil {
		log.Errorf("problem pinging the database: %v", err)
		respondWithPayload(w, http.StatusInternalServerError, &healthResponse{
			Healthy: false,
			Reason:  "database not responding",
		})

		return
	}

	respondWithPayload(w, http.StatusOK, &healthResponse{
		Healthy: true,
		Reason:  "all systems running",
	})
}
