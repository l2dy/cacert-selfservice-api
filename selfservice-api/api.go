/*
   Copyright 2019, 2020 Jan Dittberner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this program except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
//go:generate go run -tags=dev generate_assets.go

// The api package provides the CAcert self-service API implementation.
package api

import (
	"context"
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"
	"path"
	"sort"
	"strings"

	migrate "github.com/rubenv/sql-migrate"
	log "github.com/sirupsen/logrus"

	"gopkg.in/yaml.v2"

	"git.cacert.org/cacert-selfservice-api/selfservice-api/handlers"
	"git.cacert.org/cacert-selfservice-api/selfservice-api/models"
)

type ClientConfig struct {
	ClientID  string `yaml:"id"`
	PublicKey string `yaml:"key"`
}

type MailServerConfig struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}

type Config struct {
	ServerCert                   string           `yaml:"server_certificate"`
	ServerKey                    string           `yaml:"server_key"`
	HTTPSAddress                 string           `yaml:"https_address"`
	MySQLDSN                     string           `yaml:"mysql_dsn"`
	AccessLog                    string           `yaml:"access_log"`
	NotificationSenderAddress    string           `yaml:"notification_sender"`
	NotificationRecipientAddress string           `yaml:"notification_recipient_address"`
	NotificationRecipientName    string           `yaml:"notification_recipient_name"`
	MailServer                   MailServerConfig `yaml:"mail_server"`
	Clients                      []ClientConfig   `yaml:"clients"`
	DovecotHashAlgorithm         string           `yaml:"dovecot_hash_algorithm"`
}

func ReadConfig(configFile string) (*Config, error) {
	source, err := ioutil.ReadFile(configFile)
	if err != nil {
		err = fmt.Errorf("opening configuration file failed: %w", err)

		generateExampleConfig()

		return nil, err
	}

	config := &Config{}
	if err := yaml.Unmarshal(source, config); err != nil {
		return nil, fmt.Errorf("loading configuration failed: %w", err)
	}

	if config.HTTPSAddress == "" {
		config.HTTPSAddress = "127.0.0.1:8443"
	}

	if config.DovecotHashAlgorithm == "" {
		config.DovecotHashAlgorithm = "BLF-CRYPT"
	}

	for _, client := range config.Clients {
		handlers.RegisterClient(client.ClientID, client.PublicKey)
	}

	log.Info("Read configuration")

	return config, nil
}

func generateExampleConfig() {
	const (
		exampleSMTPHost = "localhost"
		exampleSMTPPort = 8025
	)

	config := &Config{
		ServerCert:                   "server.crt.pem",
		ServerKey:                    "server.key.pem",
		HTTPSAddress:                 "127.0.0.1:9443",
		MySQLDSN:                     "<username>:<password>@/cacertusers?parseTime=true",
		AccessLog:                    "access.log",
		DovecotHashAlgorithm:         "BLF-CRYPT",
		NotificationSenderAddress:    "returns@cacert.org",
		NotificationRecipientAddress: "email-admin@example.org",
		NotificationRecipientName:    "Email Admins",
		Clients: []ClientConfig{
			{
				ClientID:  "<client-id-from-client>",
				PublicKey: "-----BEGIN PUBLIC KEY-----\n<ECDSA public key data>\n-----END PUBLIC KEY-----",
			},
		},
		MailServer: MailServerConfig{
			Host: exampleSMTPHost,
			Port: exampleSMTPPort,
		},
	}

	configBytes, err := yaml.Marshal(config)
	if err != nil {
		log.Errorf("could not generate configuration data: %v", err)

		return
	}

	log.Infof("example data for config.yaml:\n\n---\n%s\n", configBytes)
}

type byID []*migrate.Migration

func (b byID) Len() int           { return len(b) }
func (b byID) Swap(i, j int)      { b[i], b[j] = b[j], b[i] }
func (b byID) Less(i, j int) bool { return b[i].Less(b[j]) }

type FilesystemSubDirMigrationSource struct {
	FileSystem http.FileSystem
	SubDir     string
}

func (f *FilesystemSubDirMigrationSource) FindMigrations() ([]*migrate.Migration, error) {
	migrations := make([]*migrate.Migration, 0)

	baseDir := fmt.Sprintf("/%s", f.SubDir)

	file, err := f.FileSystem.Open(baseDir)
	if err != nil {
		return nil, fmt.Errorf("could not open migrations directory: %w", err)
	}

	files, err := file.Readdir(0)
	if err != nil {
		return nil, fmt.Errorf("could not read migrations directory: %w", err)
	}

	for _, info := range files {
		if strings.HasSuffix(info.Name(), ".sql") {
			file, err := f.FileSystem.Open(path.Join(baseDir, info.Name()))
			if err != nil {
				return nil, fmt.Errorf("error while opening %s: %w", info.Name(), err)
			}

			migration, err := migrate.ParseMigration(info.Name(), file)
			if err != nil {
				return nil, fmt.Errorf("error while parsing %s: %w", info.Name(), err)
			}

			migrations = append(migrations, migration)
		}
	}

	// Make sure migrations are sorted
	sort.Sort(byID(migrations))

	return migrations, nil
}

func SetupDBConfig(ctx context.Context, config *Config) (*models.DbHandler, error) {
	database, err := sql.Open("mysql", config.MySQLDSN)
	if err != nil {
		return nil, fmt.Errorf("could not open database connection: %w", err)
	}

	applied, err := migrate.Exec(
		database, "mysql",
		&FilesystemSubDirMigrationSource{FileSystem: Assets, SubDir: "migrations"},
		migrate.Up)
	if err != nil {
		return nil, fmt.Errorf("running database migration failed: %w", err)
	}

	log.Infof("database migration finished, applied %d migrations", applied)

	db, err := models.NewDB(database)
	if err != nil {
		return nil, fmt.Errorf("could not create database connection: %w", err)
	}

	go func() {
		for range ctx.Done() {
			if err := db.Close(); err != nil {
				log.Errorf("problem closing the database: %v", err)
			}
		}
	}()

	return db, nil
}
