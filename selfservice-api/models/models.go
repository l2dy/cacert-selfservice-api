/*
Copyright 2019-2021 Jan Dittberner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this program except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package models

import (
	"database/sql"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"gopkg.in/gorp.v2"
)

type logType string

const (
	LogTypePasswordReset logType = "password reset"
	LogTypeCreateAccount logType = "create account"
)

type DbHandler struct {
	DbMap    *gorp.DbMap
	database *sql.DB
}

func NewDB(database *sql.DB) (*DbHandler, error) {
	handler := &DbHandler{
		database: database,
		DbMap: &gorp.DbMap{
			Db:              database,
			Dialect:         gorp.MySQLDialect{Engine: "InnoDB", Encoding: "UTF8"},
			ExpandSliceArgs: true,
		},
	}

	handler.DbMap.AddTableWithName(User{}, "user").SetKeys(true, "ID")
	handler.DbMap.AddTableWithName(Alias{}, "aliases").SetKeys(false, "User", "Alias")
	handler.DbMap.AddTableWithName(AdminLog{}, "adminlog").SetKeys(true, "ID")

	log.Info("database initialized")

	return handler, nil
}

func (d *DbHandler) Close() error {
	return d.database.Close()
}

type AdminLog struct {
	ID        int64     `db:"logID"`
	AdminName string    `db:"adminname"`
	Activity  logType   `db:"activity"`
	LogUser   string    `db:"loguser"`
	Timestamp time.Time `db:"dateline"`
	Remarks   string    `db:"remarks"`
}

func WriteAdminLog(dbMap *gorp.DbMap, logType logType, adminEmail string, username string, remarks string) error {
	user, err := FindUserByEmailAddress(dbMap, adminEmail)
	if err != nil {
		return err
	}

	adminLog := AdminLog{
		AdminName: user.Username,
		Activity:  logType,
		LogUser:   username,
		Timestamp: time.Now(),
		Remarks:   remarks,
	}

	return dbMap.Insert(&adminLog)
}

type Alias struct {
	User  string `db:"user"`
	Alias string `db:"alias"`
}

func GetAllAliases(dbMap *gorp.DbMap) ([]Alias, error) {
	aliases := make([]Alias, 0)

	_, err := dbMap.Select(&aliases, `SELECT * FROM aliases ORDER BY alias`)
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %w", err)
	}

	return aliases, nil
}

type User struct {
	ID                     int64          `db:"id"`
	Username               string         `db:"username"`
	FullNameAlias          sql.NullString `db:"fullnamealias"`
	FullNameAlias2         sql.NullString `db:"fullnamealias2"`
	Domain                 string         `db:"domain"`
	RealName               string         `db:"realname"`
	Password               string         `db:"password"`
	Role                   string         `db:"role"`
	CertificateFingerprint sql.NullString `db:"certfingerprint"`
	PGPKey                 []byte         `db:"pgpkey"`
	X509Cert               []byte         `db:"x509cert"`
}

func (u *User) GetEmailAddresses() []string {
	emailAddresses := make([]string, 0)
	emailAddresses = append(emailAddresses, fmt.Sprintf("%s@%s", u.Username, u.Domain))

	if u.FullNameAlias.Valid {
		emailAddresses = append(emailAddresses, fmt.Sprintf("%s@%s", u.FullNameAlias.String, u.Domain))
	}

	if u.FullNameAlias2.Valid {
		emailAddresses = append(emailAddresses, fmt.Sprintf("%s@%s", u.FullNameAlias2.String, u.Domain))
	}

	return emailAddresses
}

func FindUserByUsername(dbMap *gorp.DbMap, username string) (*User, error) {
	user := &User{}

	err := dbMap.SelectOne(user, `SELECT * FROM user WHERE username=:username`,
		map[string]interface{}{"username": username})
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %w", err)
	}

	return user, nil
}

func FindUserByEmailAddress(dbMap *gorp.DbMap, email string) (*User, error) {
	user := &User{}

	err := dbMap.SelectOne(user, `
SELECT *
FROM user
WHERE CONCAT(username, '@', domain) = :email
UNION
SELECT *
FROM user
WHERE fullnamealias IS NOT NULL
  AND CONCAT(fullnamealias, '@', domain) = :email
UNION
SELECT *
FROM user
WHERE fullnamealias2 IS NOT NULL
  AND CONCAT(fullnamealias2, '@', domain) = :email
`,
		map[string]interface{}{"email": email})
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %w", err)
	}

	return user, nil
}

func CheckUserExistsByUserNames(dbMap *gorp.DbMap, names []string) (bool, error) {
	userCount, err := dbMap.SelectInt(`WITH usernames AS (
	SELECT username
	FROM user
	UNION
	SELECT fullnamealias
	FROM user
	UNION
	SELECT fullnamealias2
	FROM user
)
SELECT COUNT(username)
FROM usernames
WHERE username IN (:usernames)
`, map[string]interface{}{"usernames": names})
	if err != nil {
		return false, fmt.Errorf("could not query user count for names: %w", err)
	}

	return userCount > 0, nil
}

func GetAllEmailAddresses(dbMap *gorp.DbMap, filterPattern string) ([]string, error) {
	if filterPattern == "" {
		return getAllEmailAddressesUnfiltered(dbMap)
	}

	return getAllEmailAddressesFiltered(dbMap, filterPattern)
}

func getAllEmailAddressesUnfiltered(dbMap *gorp.DbMap) ([]string, error) {
	query := `
SELECT CONCAT(username, '@', domain) AS email
FROM user
UNION
SELECT CONCAT(fullnamealias, '@', domain) AS email
FROM user
WHERE fullnamealias IS NOT NULL
  AND fullnamealias != ''
UNION
SELECT CONCAT(fullnamealias2, '@', domain) AS email
FROM user
WHERE fullnamealias2 IS NOT NULL
  AND fullnamealias2 != ''
ORDER BY email
`

	stmt, err := dbMap.Prepare(query)
	if err != nil {
		return nil, fmt.Errorf("could not prepare query: %w", err)
	}

	defer func() { _ = stmt.Close() }()

	var emails = make([]string, 0)

	// nolint:rowserrcheck
	rows, _ := stmt.Query()

	defer func() { _ = rows.Close() }()

	for rows.Next() {
		var email string

		if err := rows.Scan(&email); err != nil {
			return nil, fmt.Errorf("could not scan row: %w", err)
		}

		emails = append(emails, email)
	}

	if rows.Err() != nil {
		return nil, rows.Err()
	}

	return emails, nil
}

func getAllEmailAddressesFiltered(dbMap *gorp.DbMap, filterPattern string) ([]string, error) {
	query := `
SELECT email
FROM (
		 SELECT CONCAT(username, '@', domain) AS email
		 FROM user
		 UNION
		 SELECT CONCAT(fullnamealias, '@', domain) AS email
		 FROM user
		 WHERE fullnamealias IS NOT NULL
		   AND fullnamealias != ''
		 UNION
		 SELECT CONCAT(fullnamealias2, '@', domain) AS email
		 FROM user
		 WHERE fullnamealias2 IS NOT NULL
		   AND fullnamealias2 != ''
		 ORDER BY email
	 ) AS allemails
WHERE email LIKE ?`

	stmt, err := dbMap.Prepare(query)
	if err != nil {
		return nil, fmt.Errorf("could not prepare statement: %w", err)
	}

	defer func() { _ = stmt.Close() }()

	var emails = make([]string, 0)

	// nolint:rowserrcheck
	rows, _ := stmt.Query(fmt.Sprintf("%s%%", filterPattern))

	defer func() { _ = rows.Close() }()

	for rows.Next() {
		var email string

		if err := rows.Scan(&email); err != nil {
			return nil, fmt.Errorf("could not scan row: %w", err)
		}

		emails = append(emails, email)
	}

	if rows.Err() != nil {
		return nil, rows.Err()
	}

	return emails, nil
}

func GetAllStaffMembers(dbMap *gorp.DbMap) ([]User, error) {
	users := make([]User, 0)

	_, err := dbMap.Select(&users, `SELECT id, username, realname, role FROM user ORDER BY realname`)
	if err != nil {
		return nil, fmt.Errorf("could not execute query: %w", err)
	}

	return users, nil
}

func CreateUser(dbMap *gorp.DbMap, user *User) error {
	return dbMap.Insert(user)
}
