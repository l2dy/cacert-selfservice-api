-- +migrate Up
SET NAMES utf8mb4;
SET TIME_ZONE = '+00:00';
CREATE TABLE IF NOT EXISTS `adminlog` (
	`logID`     int(11)      NOT NULL AUTO_INCREMENT COMMENT 'ID of entry',
	`adminname` varchar(50)  NOT NULL COMMENT 'name of admin',
	`activity`  varchar(200) NOT NULL COMMENT 'activity like add or delete',
	`loguser`   varchar(50)  NOT NULL COMMENT 'user who was changed',
	`dateline`  datetime     NOT NULL COMMENT 'date and time of action',
	`remarks`   varchar(200) NOT NULL COMMENT 'remarks',
	UNIQUE KEY `logID` (`logID`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `aliases` (
	`alias` varchar(80) NOT NULL,
	`user`  varchar(80) NOT NULL,
	UNIQUE KEY `emailalias_uniq_idx` (`alias`, `user`),
	KEY `user` (`user`)
) ENGINE = MyISAM
  DEFAULT CHARSET = latin1 COMMENT ='mapping of aliases in addition to /etc/aliases that require ';

CREATE TABLE IF NOT EXISTS `user` (
	`id`              int(11)                                         NOT NULL AUTO_INCREMENT COMMENT 'uid/gid',
	`username`        varchar(40)                                     NOT NULL,
	`fullnamealias`   varchar(50) CHARACTER SET utf8 COLLATE utf8_bin          DEFAULT NULL,
	`fullnamealias2`  varchar(100) CHARACTER SET utf8 COLLATE utf8_bin         DEFAULT NULL COMMENT 'Second Fullname alias for persons with long names',
	`domain`          enum ('community.cacert.org','cacert.org')      NOT NULL DEFAULT 'cacert.org',
	`realname`        varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'used by nss and staff list',
	`password`        varchar(80)                                     NOT NULL COMMENT 'encrypt(password,"$1salt$")',
	`role`            varchar(90) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
	`certfingerprint` varchar(64) CHARACTER SET ascii COLLATE ascii_bin        DEFAULT NULL,
	`pgpkey`          blob,
	`x509cert`        blob,
	PRIMARY KEY (`id`),
	UNIQUE KEY `username` (`username`),
	KEY `certfingerprint` (`certfingerprint`(16))
) ENGINE = MyISAM
  AUTO_INCREMENT = 10132
  DEFAULT CHARSET = latin1;

CREATE VIEW IF NOT EXISTS `fullaliases` AS
SELECT `user`.`username` AS `username`, `user`.`fullnamealias` AS `fullnamealias`
FROM `user`
WHERE (`user`.`fullnamealias` IS NOT NULL);

CREATE VIEW IF NOT EXISTS `nssgrouplist` AS
SELECT `user`.`id` AS `gid`, `user`.`username` AS `username`
FROM `user`;

CREATE VIEW IF NOT EXISTS `nssgroups` AS
SELECT `user`.`username` AS `name`, `user`.`password` AS `password`, `user`.`id` AS `gid`
FROM `user`;

CREATE VIEW IF NOT EXISTS `nssuser` AS
SELECT `user`.`username`                          AS `username`,
	   `user`.`password`                          AS `password`,
	   `user`.`id`                                AS `uid`,
	   `user`.`id`                                AS `gid`,
	   `user`.`realname`                          AS `gecos`,
	   concat(_latin1'/home/', `user`.`username`) AS `homedir`,
	   _utf8'/bin/false'                          AS `shell`,
	   1                                          AS `lstchg`,
	   0                                          AS `min`,
	   99999                                      AS `max`,
	   0                                          AS `warn`,
	   0                                          AS `inact`,
	   -(1)                                       AS `expire`,
	   0                                          AS `flag`
FROM `user`;

CREATE VIEW IF NOT EXISTS `postfixcertfingerprints` AS
SELECT `user`.`certfingerprint` AS `certfingerprint`, `user`.`username` AS `username`
FROM `user`;

CREATE VIEW IF NOT EXISTS `postfixrecipients` AS
SELECT concat(`user`.`username`, _latin1'@', `user`.`domain`) AS `recipient`, `user`.`username` AS `user`
FROM `user`;

-- +migrate Down
DROP VIEW IF EXISTS `postfixrecipients`;
DROP VIEW IF EXISTS `postfixcertfingerprints`;
DROP VIEW IF EXISTS `nssuser`;
DROP VIEW IF EXISTS `nssgroups`;
DROP VIEW IF EXISTS `nssgrouplist`;
DROP VIEW IF EXISTS `fullaliases`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `aliases`;
DROP TABLE IF EXISTS `adminlog`;