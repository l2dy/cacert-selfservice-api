/*
Copyright 2019-2021 Jan Dittberner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this program except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// The services package contains services that are meant to be used by other parts of the self-service API.
package services

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"os/exec"
	"strings"
	"text/template"

	"github.com/go-mail/mail"
	log "github.com/sirupsen/logrus"
)

type headerData struct {
	name  string
	value []string
}

type headerList []headerData

type recipientData struct {
	field, address, name string
}

type notificationContent struct {
	template   string
	data       interface{}
	subject    string
	headers    headerList
	recipients []recipientData
}

type NotificationMail interface {
	GetNotificationContent() *notificationContent
}

var NotifyMailChannel = make(chan NotificationMail, 1)

type MailConfig struct {
	NotificationSenderAddress    string
	NotificationRecipientAddress string
	NotificationRecipientName    string
	MailServerHost               string
	MailServerPort               int
}

func SetupNotifications(ctx *context.Context, config *MailConfig, templateAssets *http.FileSystem) {
	quitMailChannel := make(chan int)
	go mailNotifier(config, templateAssets, quitMailChannel)

	go func() {
		for range (*ctx).Done() {
			quitMailChannel <- 1
		}
	}()
}

func mailNotifier(config *MailConfig, templateAssets *http.FileSystem, quitMailNotifier chan int) {
	log.Info("Launched mail notifier")

	for {
		select {
		case notification := <-NotifyMailChannel:
			content := notification.GetNotificationContent()

			mailText, err := renderMailTemplate(templateAssets, content.template, content.data)
			if err != nil {
				log.Errorf("rendering mail body template failed: %v", err)

				continue
			}

			signatureText, err := renderMailTemplate(templateAssets, "signature.txt", content.data)
			if err != nil {
				log.Errorf("rendering mail signature template failed: %v", err)

				continue
			}

			m := mail.NewMessage()
			m.SetAddressHeader("From", config.NotificationSenderAddress, "CAcert self service system")

			if content.recipients != nil {
				for _, recipient := range content.recipients {
					m.SetAddressHeader(recipient.field, recipient.address, recipient.name)
				}
			} else {
				m.SetAddressHeader("To", config.NotificationRecipientAddress, config.NotificationRecipientName)
			}

			m.SetHeader("Subject", content.subject)

			for _, header := range content.headers {
				m.SetHeader(header.name, header.value...)
			}

			mailBody := strings.TrimSpace(mailText.String())
			mailBody = mailBody + "\n\n-- \n" + strings.TrimSpace(signatureText.String())

			m.SetBody("text/plain", mailBody)

			d := mail.NewDialer(config.MailServerHost, config.MailServerPort, "", "")
			if err := d.DialAndSend(m); err != nil {
				log.Errorf("sending mail failed: %v", err)
			}
		case <-quitMailNotifier:
			log.Info("Ending mail notifier")

			return
		}
	}
}

func renderMailTemplate(
	templateAssets *http.FileSystem,
	templateName string,
	context interface{},
) (*bytes.Buffer, error) {
	templateData, err := (*templateAssets).Open(fmt.Sprintf("/templates/%s", templateName))
	if err != nil {
		return nil, fmt.Errorf("could not open template %s: %w", templateName, err)
	}

	b, err := ioutil.ReadAll(templateData)
	if err != nil {
		return nil, fmt.Errorf("could not read template data %s: %w", templateName, err)
	}

	t, err := template.New(templateName).Parse(string(b))
	if err != nil {
		return nil, fmt.Errorf("could not parse template %s: %w", templateName, err)
	}

	mailText := bytes.NewBufferString("")

	if err := t.Execute(mailText, context); err != nil {
		return nil, fmt.Errorf(
			"failed to execute template %s with context %+v: %w",
			templateName,
			context,
			err,
		)
	}

	return mailText, nil
}

type passwordResetNotification struct {
	Email, Username, RemoteAddress, PasswordHash string
}

func (p passwordResetNotification) GetNotificationContent() *notificationContent {
	return &notificationContent{
		template: "password_reset_notification.txt",
		data:     p,
		subject: fmt.Sprintf("[CAcert] password reset selfservice for %s by %s",
			p.Username, p.Email),
	}
}

func NewPasswordResetNotification(email, username, remoteAddress string) NotificationMail {
	return &passwordResetNotification{
		Email:         email,
		Username:      username,
		RemoteAddress: remoteAddress,
	}
}

type emailAccountCreateNotification struct {
	RealName           string
	ContactEmail       string
	UserName           string
	EmailAddresses     []string
	InitialPasswordURL string
	ValidHours         int
}

func (e emailAccountCreateNotification) GetNotificationContent() *notificationContent {
	recipients := make([]recipientData, 1)
	recipients[0] = recipientData{
		field:   "To",
		address: e.ContactEmail,
		name:    e.RealName,
	}

	return &notificationContent{
		template:   "account_creation_notification.txt",
		data:       e,
		subject:    "[CAcert] Your new cacert.org community email account",
		recipients: recipients,
	}
}

func NewEmailAccountCreateNotification(
	realName, contactEmail, userName string,
	addresses []string,
	domain, initialPasswordURL string,
	validHours int,
) NotificationMail {
	return &emailAccountCreateNotification{
		RealName:           realName,
		ContactEmail:       contactEmail,
		UserName:           userName,
		EmailAddresses:     buildEmailAddresses(addresses, domain),
		InitialPasswordURL: initialPasswordURL,
		ValidHours:         validHours,
	}
}

type emailAccountCreateAdminNotification struct {
	ContactEmail          string
	EmailAddresses        []string
	RealName              string
	Reason                string
	RequesterEmailAddress string
	UserName              string
}

func (e emailAccountCreateAdminNotification) GetNotificationContent() *notificationContent {
	return &notificationContent{
		template: "account_creation_admin_notification.txt",
		data:     e,
		subject:  fmt.Sprintf("[CAcert] new cacert.org email account for %s", e.RealName),
	}
}

func NewEmailAccountCreateAdminNotification(
	realName, username, contactEmail string, addresses []string, domain, requesterEmailAddress, reason string,
) NotificationMail {
	return &emailAccountCreateAdminNotification{
		ContactEmail:          contactEmail,
		EmailAddresses:        buildEmailAddresses(addresses, domain),
		RealName:              realName,
		Reason:                reason,
		RequesterEmailAddress: requesterEmailAddress,
		UserName:              username,
	}
}

func buildEmailAddresses(addresses []string, domain string) []string {
	emailAddresses := make([]string, 0)
	for _, localPart := range addresses {
		emailAddresses = append(emailAddresses, fmt.Sprintf("%s@%s", localPart, domain))
	}

	return emailAddresses
}

func DovecotHash(plainTextPassword string, dovecotHashAlgorithm string) (result string, err error) {
	var output bytes.Buffer

	cmd := exec.Command("/usr/bin/doveadm", "pw", "-s", dovecotHashAlgorithm)
	cmd.Stdin = bytes.NewBufferString(fmt.Sprintf("%s\n%s", plainTextPassword, plainTextPassword))
	cmd.Stdout = &output

	if err = cmd.Run(); err != nil {
		return "", fmt.Errorf("could not run doveadm pw: %w", err)
	}

	return strings.TrimSpace(output.String()), nil
}

func DovecotCreateUserMailbox(username string) (err error) {
	cmd := exec.Command("/usr/bin/sudo", "/usr/bin/doveadm", "mailbox", "create", "-u", username, "INBOX")

	if err = cmd.Run(); err != nil {
		return fmt.Errorf("could not run doveadm mailbox create: %w", err)
	}

	return nil
}
