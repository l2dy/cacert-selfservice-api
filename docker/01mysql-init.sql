CREATE DATABASE cacertusers CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';
CREATE USER 'cacertselfservice'@'%' IDENTIFIED BY 's3cr3tl0ngp4ssw0rd';
GRANT ALL PRIVILEGES ON cacertusers.* TO 'cacertselfservice'@'%';
