module git.cacert.org/cacert-selfservice-api

go 1.14

require (
	github.com/go-gorp/gorp v2.2.0+incompatible // indirect
	github.com/go-mail/mail v2.3.1+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/handlers v1.5.1
	github.com/poy/onpar v1.0.0 // indirect
	github.com/rubenv/sql-migrate v0.0.0-20200616145509-8d140a17f351
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20200824052919-0d455de96546 // indirect
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/sys v0.0.0-20210110051926-789bb1bd4061 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gorp.v2 v2.2.0
	gopkg.in/mail.v2 v2.3.1 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
