/*
Copyright 2019-2021 Jan Dittberner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this program except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"sync"

	_ "github.com/go-sql-driver/mysql"
	gorillaHandlers "github.com/gorilla/handlers"
	log "github.com/sirupsen/logrus"

	api "git.cacert.org/cacert-selfservice-api/selfservice-api"
	"git.cacert.org/cacert-selfservice-api/selfservice-api/handlers"
	"git.cacert.org/cacert-selfservice-api/selfservice-api/models"
	"git.cacert.org/cacert-selfservice-api/selfservice-api/services"
)

var version = "undefined"
var build = "undefined"

type AccessLogWriter struct {
	LogFile string
	mutex   *sync.Mutex
}

func (w *AccessLogWriter) Write(b []byte) (int, error) {
	if w.LogFile == "" {
		return os.Stdout.Write(b)
	}

	if w.mutex == nil {
		w.mutex = &sync.Mutex{}
	}

	w.mutex.Lock()

	defer func() { w.mutex.Unlock() }()

	f, err := os.OpenFile(w.LogFile, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		return 0, fmt.Errorf("could not open access log file: %w", err)
	}

	defer func() { _ = f.Close() }()

	return f.Write(b)
}

func main() {
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	log.Infof("CAcert Community Self Service API version %s, build %s", version, build)

	var configFile string

	flag.StringVar(
		&configFile, "config", "config.yaml", "Configuration file name")

	flag.Parse()

	executionContext, stopAll := context.WithCancel(context.Background())
	defer stopAll()

	config, err := api.ReadConfig(configFile)
	if err != nil {
		log.Errorf("%v", err)

		return
	}

	var db *models.DbHandler

	if db, err = api.SetupDBConfig(executionContext, config); err != nil {
		log.Errorf("error during database setup: %v", err)

		return
	}

	services.SetupNotifications(&executionContext, &services.MailConfig{
		NotificationSenderAddress:    config.NotificationSenderAddress,
		NotificationRecipientAddress: config.NotificationRecipientAddress,
		NotificationRecipientName:    config.NotificationRecipientName,
		MailServerHost:               config.MailServer.Host,
		MailServerPort:               config.MailServer.Port,
	}, &api.Assets)

	server := &http.Server{
		Addr: config.HTTPSAddress,
	}

	handlers.RegisterHandlers(db, config.DovecotHashAlgorithm)

	accessLog := &AccessLogWriter{
		LogFile: config.AccessLog,
	}

	server.Handler = gorillaHandlers.CompressHandler(
		gorillaHandlers.CombinedLoggingHandler(accessLog,
			http.DefaultServeMux))
	log.Infof("Launching application on https://%s/", server.Addr)

	if err := server.ListenAndServeTLS(config.ServerCert, config.ServerKey); err != nil {
		log.Errorf("ListenAndServerTLS failed: %v", err)

		return
	}
}
