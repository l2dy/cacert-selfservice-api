VERSION := $(shell git describe --always --dirty=-dev)
BUILD := $(shell date --iso-8601=seconds --utc)
GOFILES = $(shell find . -type f -name '*.go')
RESOURCES = $(shell find selfservice-api/resources -type f)
RESOURCE_VFS_FILE = selfservice-api/assets_vfsdata.go

all: cacert-selfservice-api

${RESOURCE_VFS_FILE}: ${RESOURCES}
	go generate -v ./...

cacert-selfservice-api: ${GOFILES} ${RESOURCE_VFS_FILE}
	go build -o $@ -x -ldflags " -X 'main.version=${VERSION}' -X 'main.build=${BUILD}'"

clean:
	rm -f cacert-selfservice-api

distclean: clean
	rm -f ${RESOURCE_VFS_FILE}

.PHONY: clean distclean all
