# CAcert community self service system API

This project contains the source code for the CAcert community self service
system API backend software.

## License

The CAcert community self service system API backend software is licensed under
the terms of the Apache License, Version 2.0.

    Copyright 2019, 2020 Jan Dittberner
 
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this program except in compliance with the License.
    You may obtain a copy of the License at
 
        http://www.apache.org/licenses/LICENSE-2.0
 
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

## History

The CAcert community self service system API backend software is a [Go]
reimplementation of the data access part of the ancient PHP implementation in
`staff.php` and `password.php` that has been running on community.cacert.org
for many years.

The user interface part is available in [a different
repository](https://git.cacert.org/cacert-selfservice.git).

---

## Development requirements

Local development requires

* golang >= 1.14
* GNU make
* doveadm
* MariaDB or MySQL server

On a Debian 10 (Buster) system with
[backports](https://backports.debian.org/Instructions/) enabled you can run the
following command to get all required dependencies:

```
sudo apt install make dovecot-core
sudo apt install -t buster-backports golang-go
```

## Getting started

Clone the code via git:

```shell script
git clone ssh://git.cacert.org/var/cache/git/cacert-selfservice-api.git
```

To get started you can build and run the application. This will generate valid
data for a `config.yaml` file and a fragment of client credentials that need to
be configured on the API server side that you will need to customize.

```shell script
make
./cacert-selfservice
```

You will also need a X.509 server certificate and a private key because the
application is accessible via TLS only.  You might use `openssl` to create a
self signed server certificate.

```shell script
openssl req -new -newkey rsa:2048 -nodes -keyout server.key.pem -x509 -out server.crt.pem -subj '/CN=localhost'
```

### Setup a local database

If you would like to have a local database for development you can use Docker
and docker-compose to get a running MariaDB server with the needed version that
is based on Debian Buster:

```shell script
docker-compose up
```

Alternatively (if you run Debian Buster) you can install a MariaDB server on
your local machine:

```shell script
sudo apt install mariadb-server mariadb-client
sudo mariadb -u root mysql -e "CREATE DATABASE cacertusers CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci'"
sudo mariadb -u root mysql -e "CREATE USER cacertselfservice@localhost IDENTIFIED BY 's3cr3tl0ngp4ssw0rd'"
sudo mariadb -u root mysql -e "GRANT ALL PRIVILEGES ON cacertusers.* TO cacertselfservice@localhost"
```

The database schema will be initialized/upgraded when the API server starts
with a valid configuration that specifies the database.

### Customize `config.yaml`

You can use the following table to find useful values for the parameters in `config.yaml`.

Parameter | Description | How to get a valid value
----------|-------------|-------------------------
`server_certificate` | Server certificate for TLS | use the `server.crt.pem` from above or a properly CA signed certificate for a real system
`server_key` | Server private key for TLS | use the `server.key.pem` from above
`https_address` | listening address for HTTPS | use `:443` for production, keep the generated value for a local setup
`mysql_dsn` | MySQL DSN | see [the usage instructions for the MySQL driver], use credentials from above
`access_log` | Path to the HTTP access log file | a file path. If this is not set access logs are written to the standard output
`dovecot_hash` | The algorithm that is used for password hashing | use one of the values from the output of `doveadm pw -l` the default value is `BLF-CRYPT`
`notification_sender` | Sender address for notification mails | use a mail address that should be used as sender address
`notification_recipient_address` | Default recipient address for notification mails | use a mail address the should be used as default recipient address
`notification_recipient_name` | Default recipient name for notification mails | use a descriptive name for recipients
`mail_server.host` | hostname of the outgoing SMTP server | `localhost` or the name of your SMTP relay
`mail_server.port` | TCP port number of the outgoing SMTP server | `25` or the port number of a [debugging smtp server](#debugging-smtp-server)
`clients` | a list of client credentials | use the output of the `cacert-selfservice` software when it is started for the first time

### Debugging SMTP server

You can use
[aiosmtpd](https://aiosmtpd.readthedocs.io/en/latest/aiosmtpd/docs/cli.html) to
setup a small testing SMTP server that logs to stdout:

```shell script
sudo apt install python3-aiosmtpd
python3 -m aiosmtpd -n
```

## Code structure

```
.
├── selfservice-api/
│   ├── handlers/
│   ├── models/
│   ├── resources/
│   │   ├── migrations/
│   │   ├── templates/
│   │   ├── assets_fs.go
│   │   ├── generate_assets.go
│   │   ├── migrations.go
│   ├── services/
│   └── configuration.go
├── config.yaml.example
├── go.mod
├── go.sum
├── Jenkinsfile
├── LICENSE
├── Makefile
├── README.md
└── selfservice-api.go
```

The `selfservice-api` directory contains the application code.
Database migration and email templates are contained in the `resources` package.
The HTTP handlers are contained in the `handlers` package.
Access to the database is implemented in the `models` package.
The `services` package contains the mail notification service.
The entry point into the application is in the `selfservice-api.go` file in the top level directory.

`Makefile` controls the build. `Jenkinsfile` contains the pipeline definition for the [Continuous Integration Job].

[Continuous Integration Job]: https://jenkins.cacert.org/job/cacert-selfservice-api/
[Go]: https://golang.org/
[the usage instructions for the MySQL driver]: https://github.com/go-sql-driver/mysql#usage
